// Note: For all file operations use promises with fs

// Q1. Create 2 files simultaneously (without chaining two function calls one after the other).
// Wait for 2 seconds and starts deleting them one after another.
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)
const fs = require("fs/promises");
const path = require("path");

function createRandomFiles(number) {
  return new Promise((resolve, reject) => {
    const filesArray = Array(number)
      .fill(0)
      .map((item) => {
        return `${parseInt(Math.random() * 10)}.json`;
      });
    const createArray = [];
    for (let index = 0; index < filesArray.length; index++) {
      createArray.push(
        fs.writeFile(path.join(__dirname, filesArray[index]), "Random")
      );
    }

    return Promise.allSettled(createArray)
      .then(() => {
        console.log("Files created");
        resolve(filesArray);
      })
      .catch((err) => {
        reject(err);
      });
  });
}

function deleteFile(fileName) {
  return fs.unlink(path.join(__dirname, fileName));
}

function createAndDeleteFiles() {
  return createRandomFiles(2)
    .then((filesArray) => {
      return new Promise((resolve) => {
        setTimeout(() => {
          resolve(filesArray);
        }, 2 * 1000);
      });
    })

    .then((filesArray) => {
      return new Promise((resolve) => {
        resolve(filesArray);
        return deleteFile(filesArray[0]);
      });
    })

    .then((filesArray) => {
      return deleteFile(filesArray[1]);
    })

    .catch((err) => {
      console.log(err);
    });
}

// createAndDeleteFiles().then(() => {
//   console.log("Files deleted");
// });

// Q2. Create a new file with lipsum data (you can google and get this).
// Do File Read and write data to another file
// Delete the original file

// A. using promises chaining
function createFile(data, fileName) {
  return fs.writeFile(path.join(__dirname, fileName), data);
}

function readFile(fileName) {
  return fs.readFile(path.join(__dirname, fileName));
}

function problem2() {
  return fetch("https://baconipsum.com/api/?type=meat-and-filler")
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error("404 Error");
      }
    })

    .then((data) => {
      return new Promise((resolve, reject) => {
        const fileName = "original.txt";
        createFile(data, fileName)
          .then(() => {
            console.log("Created Original File");
            resolve(fileName);
          })
          .catch((err) => {
            reject(err);
          });
      });
    })

    .then((fileName) => {
      return readFile(fileName);
    })

    .then((data) => {
      const fileName2 = "duplicate.txt";
      createFile(data, fileName2);
    })

    .then(() => {
      console.log("Created duplicate file");

      //deleteFile was defined in early problem
      return deleteFile("original.txt");
    })

    .catch((err) => {
      console.log(err);
    });
}

// problem2().then(() => {
//   console.log("Original deleted");
// });

// B. using callbacks
const fsNew = require("fs");

function callbackProblem2() {
  return fetch("https://baconipsum.com/api/?type=meat-and-filler")
    .then((response) => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error("404 Error");
      }
    })

    .then((data) => {
      fsNew.writeFile(
        path.join(__dirname, "original.txt"),
        JSON.stringify(data),
        (err) => {
          if (err) {
            throw err;
          } else {
            console.log("Original created");

            fsNew.readFile(
              path.join(__dirname, "original.txt"),
              (err, data) => {
                if (err) {
                  throw err;
                } else {
                  fsNew.writeFile(
                    path.join(__dirname, "duplicate.txt"),
                    data,
                    (err) => {
                      if (err) {
                        throw err;
                      } else {
                        console.log("Duplicate created");

                        fsNew.unlink(
                          path.join(__dirname, "original.txt"),
                          (err) => {
                            if (err) {
                              throw err;
                            } else {
                              console.log("Original deleted");
                            }
                          }
                        );
                      }
                    }
                  );
                }
              }
            );
          }
        }
      );
    })

    .catch((err) => {
      console.log(err);
    });
}

// callbackProblem2();

// Q3.
function login(user, val) {
  if (val % 2 === 0) {
    return Promise.resolve(user);
  } else {
    return Promise.reject(new Error("User not found"));
  }
}

function getData() {
  return Promise.resolve([
    {
      id: 1,
      name: "Test",
    },
    {
      id: 2,
      name: "Test 2",
    },
  ]);
}

function logData(user, activity) {
  fs.appendFile(
    path.join(__dirname, "Activities"),
    `${user}: ${activity} ${new Date()} \n`
  )
    .then(() => {
      console.log("Activity Recorded");
    })
    .catch((err) => {
      console.log(err);
    });

  //     // use promises and fs to save activity in some file
}

// Use appropriate methods to
// A. login with value 3 and call getData once login is successful
login("John", 3)
  .then((user) => {
    getData()
      .then((users) => {
        console.log(
          users.filter((user) => {
            return user.id === 3;
          })
        );
      })

      .catch((err) => {
        console.log(err);
      });
  })
  .catch((err) => {
    console.log(err);
  });
// B. Write logic to logData after each activity for each user. Following are the list of activities
//     "Login Success"
//     "Login Failure"
//     "GetData Success"
//     "GetData Failure"

function getActivity(user, value) {
  return login(user, value)
    .then((user) => {
      logData(user, "Login Success");
      getData()
        .then((users) => {
          const userValid = users.reduce((accumulator, current) => {
            if (current.id === value && current.name === user) {
              accumulator = 1;
            }
            return accumulator;
          }, 0);
          if (userValid) {
            logData(user, "GetData Success");
          } else {
            throw new Error("GetData Failure");
          }
        })

        .catch((err) => {
          logData(user, "GetData Failure");
          console.log(err);
        });
    })

    .catch((err) => {
      logData(user, "Login Failure");
      console.log(err);
    });
}

getActivity("Test 2", 2);
//     Call log data function after each activity to log that activity in the file.
//     All logged activity must also include the timestamp for that activity.

//     You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
//     All calls must be chained unless mentioned otherwise.
